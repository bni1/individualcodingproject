/**********************************************
* File: p2.cpp
* Author: Bo Ni
* Email: bni@nd.edu
* Solution to Problem 2.
* Problem Description: You are given a list of projects and dependencies (which is a list of pairs of projects, where the second project is 
dependent on the first project). All of a project' dependencies must be build before the project is begun. Find a build order that will allow
the projects to be built.
* It includes a main driver and a function to find the order
**********************************************/

#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
using namespace std;


class Solution {
public: 
    /********************************************
    * Function Name  : findOrder
    * Pre-conditions : vector<pair<int, int> >
    * Post-conditions: vector<int>
    *  
    * Function to find the order of project that will compile. The first project is depended on the second project.
    ********************************************/
    vector<int> findOrder(int numProjects, vector<pair<int, int> >& projects) {
        // we represent the projects as a directed graph by adjacency list
        graph = vector<vector<int> >(numProjects);
        for(auto p:projects) graph[p.second].push_back(p.first);
        vector<int> ans;
        vector<int> status(numProjects, 0);
        for(int i = 0; i < numProjects; ++i) {
            if(dfs(i, status, ans)) return {};
        }
        reverse(ans.begin(), ans.end());
        return ans;
    }
private:

    vector<vector<int> > graph;
    // Status encoding: 0: unknown, 1: visiting, 2: visited

    /********************************************
    * Function Name  : dfs
    * Pre-conditions : int, vector<int>& s, vector<int>& ans
    * Post-conditions: bool
    *  
    * dfs helper function for findOrder
    ********************************************/
    bool dfs(int i, vector<int>& s, vector<int>& ans) {
        if(s[i] == 2) return false;
        if(s[i] == 1) return true;
        s[i] = 1;
        for(auto t : graph[i]) {
            if(dfs(t, s, ans)) return true;
        }
        s[i] = 2;
        ans.push_back(i);
        return false;
    }
};

/********************************************
* Function Name  : prettyPrint
* Pre-conditions : vector<int>
* Post-conditions: void
*  
* Prints out a vector beautifully
********************************************/
void prettyPrint(vector<int>);

/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
*  
* Main driver of the program that includes several test cases
********************************************/
int main() {
    Solution p2;
    vector<pair<int, int> > test1{pair<int, int>{1, 0}, pair<int, int>{2, 0}, pair<int, int>{3, 1}, pair<int, int>{3, 2}};
    vector<int> ans = p2.findOrder(4, test1);
    cout << "For the test case [[1, 0], [2, 0], [3, 1], [3, 2]], the program yields the answer: " << endl;
    prettyPrint(ans);

    vector<pair<int, int> > test2{pair<int, int>{0, 2}, pair<int, int>{1, 0}, pair<int, int>{3, 1}, pair<int, int>{3, 2}};
    ans = p2.findOrder(4, test2);
    cout << "For the test case [[0, 2], [1, 0], [3, 1], [3, 2]], the program yields the answer: " << endl;
    prettyPrint(ans);

    vector<pair<int, int> > test3{pair<int, int>{1, 0}, pair<int, int>{2, 1}};
    ans = p2.findOrder(3, test3);
    cout << "For the test case [[1, 0], [2, 1]], the program yields the answer: " << endl;
    prettyPrint(ans);
    return 0;
    
}

void prettyPrint(vector<int> vec) {
    for (int i = 0; i < vec.size(); ++i) {
        if(i != vec.size()-1) {
            cout << vec[i] << "->";
        }
        else cout << vec[i] << endl;
    }
}


