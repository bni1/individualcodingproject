#include <unordered_set>
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
using namespace std;

string findWord(string, vector<string>*);

int main() {
    // Import Data
    ifstream ifs;
    string word; 
    vector<string> dict;
    ifs.open("words.txt");
    while(!ifs.eof()) {
        getline(ifs, word);
        dict.push_back(word);
    }
    sort(dict.begin(), dict.end(), [](const string& a, const string& b) {
        return a.size() < b.size();
    });
    // Test Cases
    cout << "Shortest Word For " <<  "RCT 100SA is " << findWord("RCT 100SA", &dict) << endl;
    // cout << "Shortest Word For " <<  "RT 123SO is " << findWord("RT 123SO", &dict) << endl;
    // cout << "Shortest Word For " <<  "AQ 10S0K is " << findWord("AQ 10S0K", &dict) << endl;
    // cout << "Shortest Word For " <<  "TNT 055RB is " << findWord("TNT 055RB", &dict) << endl;
    // cout << "Shortest Word For " <<  "AEI 1O2U3 is " << findWord("AEI 1O2U3", &dict) << endl;
    // cout << "Shortest Word For " <<  "AM 1E2D3 is " << findWord("AM 1E2D3", &dict) << endl;
    return 0;
}

string findWord(string plate, vector<string>* dict) {
    bool tempBool;
    plate.erase(remove_if(plate.begin(), plate.end(), 
    [](char i) {
        return !isalpha(i);
    }), plate.end());
    for(int i = 0; i < dict->size(); ++i) {
        string word = dict->at(i);
        tempBool = true;
        unordered_set<int> indices;
        for(char c : plate) {
            if(word.find(c) == string::npos) tempBool = false;
            else {
                for(int j = 0; j < word.size(); ++j) {
                    if(c == word[j]){
                        word.erase(word.begin() + j);
                    }
                }
            }
        }
        if(tempBool == true) return dict->at(i);
    }
    return "No Words Found";
}

