/**********************************************
* File: testP2.cpp
* Author: Bo Ni
* Email: bni@nd.edu
*  
**********************************************/


#include "problem2.h"

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for testing 
* problem2
********************************************/

int main(int argc, char *argv[]) {
    if (argc != 2) {
        cout << "Please Enter One Eligible File Name" << endl;
        return 1;
    }
    Problem2 p2 = Problem2();
    cout << "Please enter the word that you are interested in counting: ";
    string in;
    cin >>  in; 
    cout << p2.findFrequency(argv[1], in) << endl;
    return 0;
}