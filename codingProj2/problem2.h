/**********************************************
* File: problem2.h
* Author: Bo Ni
* Email: bni@nd.edu
* Header for the problem 2 class. Function headers
* are in the problem2.cpp file
**********************************************/

#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>

using namespace std;
class Problem2 {
public:
    Problem2();
    int findFrequency (string filename, string target);
};