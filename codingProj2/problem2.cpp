/**********************************************
* File: problem2.cpp
* Author: Bo Ni
* Email: bni@nd.edu
* Implementation of the problem 2 class. It includes
* a default constructor and some functions
**********************************************/

#include "problem2.h"
#include <ctype.h>
void tolower(string&);

/********************************************
* Function Name  : Problem2
* Pre-conditions : none
* Post-conditions: none
*  
* Empty Constructor
********************************************/
Problem2::Problem2() {};

/********************************************
* Function Name  : findFrequency
* Pre-conditions : string, string
* Post-conditions: int
*  
* The function that counts the frequency of
* a given word in a text file
********************************************/
int Problem2::findFrequency (string filename, string target) {
        tolower(target);
        int res = 0;
        ifstream inFile;
        inFile.open(filename);
        if(!inFile) {
            cerr << "unable to open file " << filename << endl;
        }
        string word;
        while (inFile >> word) {
            tolower(word);
            if(word == target) res++; 
        }
        return res;
    }

/********************************************
* Function Name  : tolower
* Pre-conditions : string&
* Post-conditions: void
*  
* Function for converting a string to its
* lower case
********************************************/
void tolower(string& word) {
    for(int i = 0; i < word.size(); ++i) {
        if (word[i] > 'A' && word[i] < 'Z') {
            word[i] = tolower(word[i]);
        }
    }
}